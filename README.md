# Projeto Teste Builders!

Desenvolva um projeto Java com Spring Boot (utilizando qualquer modulo que achar necessário), que possua uma api REST com CRUD de Cliente (id, nome, cpf, dataNascimento). O CRUD deve possuir uma api de GET, POST, DELETE, PATCH e PUT.

A api de GET deve aceitar query strings para pesquisar os clientes por CPF e nome. Também é necessário que nessa api os clientes voltem paginados e que possua um campo por cliente com a idade calculado dele considerando a data de nascimento.

O banco de dados deve estar em uma imagem Docker ou em um sandbox SAAS (banco SQL).


O projeto deve estar em um repositório no BitBucket e na raiz do projeto deve conter um Postman para apreciação da api.

# Para rodar a aplicação Docker

Favor baixar no repositorio o projeto que tambem contem as configurações para subir a aplicação no docker.

	$ git clone https://byteanalysis@bitbucket.org/byteanalysis/builders-test.git

Entre na pasta que consta o arquivo docker-compose.yml

	$ CD [pasta do projeto, onde esta o arquivo docker-compose.yml]

Para subir a aplicação basta executar o comando

	$ docker-compose up -d --build

********************
### Para consumir os endpoints coloquei o arquivo buildes.postman_collection.json com a exportação do postman com o consumo.
********************
Caso tenha algum problema segue os links e os jsons.

## Post
	
	http://localhost:9000/api/client

### body

	{
		"nome":"Ali Ahmad Hassan",
		"cpf": "33486173839",
		"dataNascimento": "1985-01-01"
	}

## Put
	
	http://localhost:9000/api/client

### body

	{
		"id": 1,
		"nome":"Ali Hassan",
		"cpf": "33486173839",
		"dataNascimento": "1985-01-01"
	}

## Patch
	
	http://localhost:9000/api/client/1

### body

	{
		"nome":"Ali Ahmad Hassan",
		"dataNascimento": "1985-01-01"
	}

## Delete

	http://localhost:9000/api/client/1

## Get - All

	http://localhost:9000/api/client/0/100

## Get - Id

	http://localhost:9000/api/client/1
	
## Get - Nome

	http://localhost:9000/api/client/byName/0/100/Al

## Get - CPF

	http://localhost:9000/api/client/bySocialSecurityNumber/0/100/12
