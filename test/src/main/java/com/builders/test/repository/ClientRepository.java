package com.builders.test.repository;

import org.springframework.data.domain.*;
import org.springframework.data.repository.CrudRepository;

import com.builders.test.entity.Client;

public interface ClientRepository extends CrudRepository<Client, Integer> {
	
	Page<Client> findAll(Pageable pages);
	
	Page<Client> findAllByNameIgnoreCaseContaining(Pageable pages, String name);
	
	Page<Client> findAllBySocialSecurityNumberIgnoreCaseContaining(Pageable pages, String socialSecurityNumber);
}