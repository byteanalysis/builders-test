package com.builders.test.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import com.builders.test.entity.Client;
import com.builders.test.entity.patch.ClientPatch;
import com.builders.test.response.Response;
import com.builders.test.service.ClientService;

@RestController
@RequestMapping("/api/client")
@CrossOrigin(origins = "*")
public class ClientController {

	@Autowired
	private ClientService clientService;
	
	@PostMapping()
	public ResponseEntity<Response<Client>> create(HttpServletRequest request, @RequestBody Client client,
			BindingResult result){
		Response<Client> response = new Response<Client>();
		
		try {
			validateCreateUpdateClient(client, result);
			
			if(result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));;
				return ResponseEntity.badRequest().body(response);
			}
			
			Client clientPersisted = clientService.createOrUpdate(client);
			response.setData(clientPersisted);
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.badRequest().body(response);
		}
		
		
		return ResponseEntity.ok(response);
	}

	@PutMapping()
	public ResponseEntity<Response<Client>> update(HttpServletRequest request, @RequestBody Client client,
			BindingResult result){
		
		Response<Client> response = new Response<Client>();
		
		try {
			validateCreateUpdateClient(client, result);
			
			if(result.hasErrors()) {
				result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));;
				return ResponseEntity.badRequest().body(response);
			}
			
			Client clientPersisted = clientService.createOrUpdate(client);
			response.setData(clientPersisted);
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		
		
		return ResponseEntity.ok(response);
	}
	
	@PatchMapping("{id}")
	public ResponseEntity<Response<Client>> patchUpdate(HttpServletRequest request, @RequestBody ClientPatch client,
			@PathVariable("id") Integer id, BindingResult result){
		
		Response<Client> response = new Response<Client>();
		
		try {
			if(client.getBorn() == null && client.getName() == null) {
				result.addError(new ObjectError("Group", "ClientPatch is invalid"));
				return ResponseEntity.badRequest().body(response);
			} else {
				Client clientPersisted = clientService.patchUpdate(client, id);
				response.setData(clientPersisted);
			}
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("{id}")
	public ResponseEntity<Response<Client>> findById(@PathVariable("id") Integer id){
		Response<Client> response = new Response<Client>();
		
		try {
			Client client = this.clientService.findById(id);

			if(client == null) 
				return ResponseEntity.notFound().build();
			
			response.setData(client);
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		return ResponseEntity.ok(response);		
	}
	
	@DeleteMapping("{id}")
	public ResponseEntity<Response<Client>> delete(@PathVariable("id") Integer id){
		Response<Client> response = new Response<Client>();
		
		try {
			Client client = this.clientService.findById(id);
			
			if(client == null) 
				return ResponseEntity.notFound().build();
			
			
			this.clientService.delete(id);
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}
		return ResponseEntity.ok(response);		
	}
	
	@GetMapping(value = "{page}/{count}")
	public ResponseEntity<Response<Page<Client>>> findAll(HttpServletRequest request,
			@PathVariable("page") Integer page, @PathVariable("count") Integer count) {
		Response<Page<Client>> response = new Response<Page<Client>>();
		
		try {
			Page<Client> clients = this.clientService.findAll(page, count);
			
			if(clients.isEmpty())
				return ResponseEntity.notFound().build();
			response.setData(clients);
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "byName/{page}/{count}/{name}")
	public ResponseEntity<Response<Page<Client>>> findAllByName(HttpServletRequest request,
			@PathVariable("page") Integer page, @PathVariable("count") Integer count,
			@PathVariable("name") String name) {
		Response<Page<Client>> response = new Response<Page<Client>>();
		
		try {
			Page<Client> clients = this.clientService.findAllByName(page, count, name);
			
			if(clients.isEmpty())
				return ResponseEntity.notFound().build();
			
			response.setData(clients);
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		return ResponseEntity.ok(response);
	}
	
	@GetMapping(value = "bySocialSecurityNumber/{page}/{count}/{socialSecurityNumber}")
	public ResponseEntity<Response<Page<Client>>> findAllBySocialSecurityNumber(HttpServletRequest request,
			@PathVariable("page") Integer page, @PathVariable("count") Integer count,
			@PathVariable("socialSecurityNumber") String socialSecurityNumber) {
		Response<Page<Client>> response = new Response<Page<Client>>();
		
		try {
			Page<Client> clients = this.clientService.findAllBySocialSecurityNumber(page, count, socialSecurityNumber);
			
			if(clients.isEmpty())
				return ResponseEntity.notFound().build();
			
			response.setData(clients);
		} catch (Exception e){
			response.getErrors().add(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(response);
		}

		return ResponseEntity.ok(response);
	}

	private void validateCreateUpdateClient(Client client, BindingResult result) {
		if(client.getName().equals(""))
			result.addError(new ObjectError("Group", "Name not informated"));

		if(client.getSocialSecurityNumber().equals(""))
			result.addError(new ObjectError("Group", "Social Security Number not informated"));
		else
			if(! com.builders.test.common.ValidationService.isBrazilianSocialSecurityNumber(client.getSocialSecurityNumber())) {
				result.addError(new ObjectError("Group", "Social Security Number '"+client.getSocialSecurityNumber()+"' is invalid"));
			}
		
		if(client.getBorn() == null)
			result.addError(new ObjectError("Group", "Born not informated"));
	}
	
}
