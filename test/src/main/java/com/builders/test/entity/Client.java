package com.builders.test.entity;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Client {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(length=150)
	@JsonProperty("nome")
	private String name;
	
	@Column(length=11)
	@JsonProperty("cpf")
	private String socialSecurityNumber;
	
	@JsonProperty("dataNascimento")
	private Date born;
	
	@Transient
	@JsonProperty("idade")
	private int yearOld;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSocialSecurityNumber() {
		return socialSecurityNumber;
	}

	public void setSocialSecurityNumber(String socialSecurityNumber) {
		this.socialSecurityNumber = socialSecurityNumber;
	}

	public Date getBorn() {
		return born;
	}

	public void setBorn(Date born) {
		this.born = born;
	}

	public int getYearOld() {
	    Calendar bornDate = Calendar.getInstance();  
	    bornDate.setTime(this.born); 
	    Calendar today = Calendar.getInstance();  

	    this.yearOld = today.get(Calendar.YEAR) - bornDate.get(Calendar.YEAR); 

	    if (today.get(Calendar.MONTH) < bornDate.get(Calendar.MONTH)) {
	    	this.yearOld--;  
	    } 
	    else 
	    { 
	        if (today.get(Calendar.MONTH) == bornDate.get(Calendar.MONTH) && today.get(Calendar.DAY_OF_MONTH) < bornDate.get(Calendar.DAY_OF_MONTH)) {
	        	this.yearOld--; 
	        }
	    }
		
		return this.yearOld;
	}

}
