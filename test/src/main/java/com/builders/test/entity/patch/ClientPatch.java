package com.builders.test.entity.patch;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientPatch {

	@JsonProperty("nome")
	private String name;
	
	@JsonProperty("dataNascimento")
	private Date born;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBorn() {
		return born;
	}

	public void setBorn(Date born) {
		this.born = born;
	}
}
