package com.builders.test.common;

import java.util.InputMismatchException;

public class ValidationService {
	
	public static boolean isBrazilianSocialSecurityNumber(String socialSecurityNumber) {
		 // considera-se erro CPF's formados por uma sequencia de numeros iguais
        if (socialSecurityNumber.equals("00000000000") ||
            socialSecurityNumber.equals("11111111111") ||
            socialSecurityNumber.equals("22222222222") || socialSecurityNumber.equals("33333333333") ||
            socialSecurityNumber.equals("44444444444") || socialSecurityNumber.equals("55555555555") ||
            socialSecurityNumber.equals("66666666666") || socialSecurityNumber.equals("77777777777") ||
            socialSecurityNumber.equals("88888888888") || socialSecurityNumber.equals("99999999999") ||
            (socialSecurityNumber.length() != 11))
            return(false);
          
        char dig10, dig11;
        int sm, i, r, num, weight;
          
        try {
            sm = 0;
            weight = 10;
            for (i=0; i<9; i++) {              

            	num = (int)(socialSecurityNumber.charAt(i) - 48); 
            sm = sm + (num * weight);
            weight = weight - 1;
            }
          
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                dig10 = '0';
            else dig10 = (char)(r + 48); 
          
            sm = 0;
            weight = 11;
            for(i=0; i<10; i++) {
            num = (int)(socialSecurityNumber.charAt(i) - 48);
            sm = sm + (num * weight);
            weight = weight - 1;
            }
          
            r = 11 - (sm % 11);
            if ((r == 10) || (r == 11))
                 dig11 = '0';
            else dig11 = (char)(r + 48);
          

            if ((dig10 == socialSecurityNumber.charAt(9)) && (dig11 == socialSecurityNumber.charAt(10)))
                 return(true);
            else return(false);
                } catch (InputMismatchException erro) {
                return(false);
            }
	}
}
