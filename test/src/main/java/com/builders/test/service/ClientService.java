package com.builders.test.service;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import com.builders.test.entity.Client;
import com.builders.test.entity.patch.ClientPatch;

@Component
public interface ClientService {
	Client createOrUpdate(Client entity);
	
	Client patchUpdate(ClientPatch clientPatch, Integer id);
	
	Client findById(Integer id);
	
	void delete(Integer id);
	
	Page<Client> findAll(Integer page, Integer count);
	
	Page<Client> findAllByName(Integer page, Integer count, String name);
	
	Page<Client> findAllBySocialSecurityNumber(Integer page, Integer count, String socialSecurityNumber);
}
