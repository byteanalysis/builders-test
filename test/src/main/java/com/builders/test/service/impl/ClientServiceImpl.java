package com.builders.test.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Component;

import com.builders.test.entity.Client;
import com.builders.test.entity.patch.ClientPatch;
import com.builders.test.repository.ClientRepository;
import com.builders.test.service.ClientService;

@Component
public class ClientServiceImpl implements ClientService {

	@Autowired
	private ClientRepository repository;
	
	@Override
	public Client createOrUpdate(Client entity) {
		return this.repository.save(entity);
	}

	@Override
	public Client findById(Integer id) {
		return this.repository.findById(id).orElse(null);
	}

	@Override
	public void delete(Integer id) {
		this.repository.deleteById(id);
	}

	@Override
	public Page<Client> findAll(Integer page, Integer count) {
		Pageable pages = PageRequest.of(page, count);
		
		return this.repository.findAll(pages);
	}

	@Override
	public Page<Client> findAllByName(Integer page, Integer count, String name) {
		Pageable pages = PageRequest.of(page, count);
		
		return this.repository.findAllByNameIgnoreCaseContaining(pages, name);
	}

	@Override
	public Page<Client> findAllBySocialSecurityNumber(Integer page, Integer count, String socialSecurityNumber) {
		Pageable pages = PageRequest.of(page, count);
		
		return this.repository.findAllBySocialSecurityNumberIgnoreCaseContaining(pages, socialSecurityNumber);
	}

	@Override
	public Client patchUpdate(ClientPatch clientPatch, Integer id) {
		Client client = findById(id);
		
		if(clientPatch.getName() != null || !clientPatch.getName().equals(""))
			client.setName(clientPatch.getName());
		if(clientPatch.getBorn() != null)
			client.setBorn(clientPatch.getBorn());
		
		return this.repository.save(client);
	}

}
